public class Int2BinRec {

    public static String intToBin(int integer) {
	if (integer == 0 || integer == 1)
	    return integer + "";
	else
	    return intToBin(integer/2) + (integer%2);
    }
    
    public static void main(String args[]) {

	int integer = Integer.parseInt(args[0]);

	System.out.println(intToBin(integer));
	
    }
}
