public class Int2BinLoop {

    public static void main(String args[]) {

	String strbin = "";
	int integer = Integer.parseInt(args[0]);
	
	while (integer > 0) {
	    strbin = ((integer % 2) == 0 ? "0" : "1") + strbin;
	    integer = integer / 2;
	}

	System.out.println(strbin);
    }
}
