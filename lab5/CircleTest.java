public class CircleTest {

	public static void main(String[] args) {

		Circle circle;
		circle = new Circle();
		
		circle.radius = 10;
		
		System.out.println("Area of circle is: " + circle.area());
		System.out.println("Perimeter of circle is: " + circle.perimeter());
	}

}
