
public class RectangleTest {

	public static void main(String[] args) {
		
		Rectangle rectangle;
		rectangle = new Rectangle();
		
		rectangle.sideA = 5;
		rectangle.sideB = 6;
		
		System.out.println("Area of rectangle is: " + rectangle.area());
		System.out.println("Perimeter of rectangle is: " + rectangle.perimeter());
	}

}
