public class Rectangle {
	
	public int sideA;
	public int sideB;
	
	public double area() {
		return sideA * sideB;
	}
	
	public double perimeter() {
		return (sideA * 2) + (sideB * 2);
	}

}
