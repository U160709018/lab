
public class Circle {
	
	public int radius;
	
	public double area() {
		
		return Math.PI * radius * radius;
	}
	
	public double perimeter() {
		
		return Math.PI * radius * 2;
	}

}
