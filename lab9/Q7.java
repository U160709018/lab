
public class Q7 {

	public static void main(String[] args) {
		System.out.println(powerN(3,2));
	}
	
	public static int powerN(int N, int n) {
		if (n == 0)
			return 1;
		else
			return N * powerN(N, n-1);
	}
}
