public class Q2 {
	
	private static int min(int a, int b, int c) {
		int temp = 0;
		
		temp = (a < b) ? a : b;
		temp = (temp < c) ? temp : c;
		
		return temp;
	}
}
