package q10_11_12;

public class Box extends Rectangle{
	
	private int height;
	
	public Box(int h, int w, int l) {
		super(l, w);
		this.height = h;
	}
	
	public double area() {
		return  2 * super.area() +
				2 * this.height * super.width +
				2 * this.height * super.length;
	}
}
