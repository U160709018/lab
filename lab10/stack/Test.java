package stack;

public class Test {
	
	public static <T> void main(String[] args) {
		StackArrayListImpl<T> stack = new StackArrayListImpl<T>();
		
		stack.push("Hi");
		stack.push(123);
		stack.push(3.14);
		stack.push("Hello");
		
		while (stack.isEmpty() == false) {
			System.out.println("Popping item from stack: " + stack.pop());
			
			if (stack.isEmpty() == true)
				System.out.println("Stack is Empty");
		}
		
		StackImpl<String> stack2 = new StackImpl<String>();
		
		stack2.push("Houston");
		stack2.push("Charlie");
		stack2.push("Queen");
		
		Object obj = stack2.pop();
		System.out.println(obj);
		
		System.out.println(stack2.toList());
	}
}
