package emre.shapes;

public class Circle {

	protected int radius;
	
	public Circle(int radius) {
		this.radius = radius;
	}
	
	public double area() {
		return Math.PI * radius * radius; 
	}
}
