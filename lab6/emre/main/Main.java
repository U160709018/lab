package emre.main;

import emre.shapes.Circle;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		ArrayList<Circle> circles = new ArrayList<Circle>();
			
	
		circles.add(new Circle(4));
		circles.add(new Circle(7));
		circles.add(new Circle(9));
		
		for (int i=0; i < circles.size(); i++) {
			Circle c = circles.get(i);
			System.out.println(c.area());
		}
		
	}
}
