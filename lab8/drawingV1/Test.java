package drawingV1;

public class Test {

	public static void main(String[] args) {
		Drawing drawing = new Drawing();
		drawing.addCircle(new Circle(5));
		drawing.addRectangle(new Rectangle(3,6));
		drawing.addCircle(new Circle(9));
		drawing.addRectangle(new Rectangle(2,6));
		
		System.out.println(drawing.calculateTotalArea());

	}

}
