public class MatrixCalculator {
    public static void main(String args[]) {

	int[][] matrixA = {{6, 8, 2},
			   {9, 5, 11},
			   {7, 2, 5}};

	int[][] matrixB = {{4, 6, 3},
			   {5, 8, 1},
			   {6, 6, 7}};

	int sum[][] = add(matrixA, matrixB);
	
	for (int i=0; i < 3; i++) {
	    for (int j=0; j < 3; j++) {
		System.out.print(sum[i][j] +", ");
	    }
	    System.out.println();
	}
	
    }

    public static int[][] add(int [][] matrix1, int [][] matrix2) {
	int [][] result = new int[3][3];

	for (int i=0; i < 3; i++) {
	    for (int j=0; j < 3; j++) {
		result[i][j] = matrix1[i][j] + matrix2[i][j];
	    }
	}

	return result;
    }
}
