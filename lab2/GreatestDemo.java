public class GreatestDemo {
    public static void main(String args[]) {
	
	int value1 = Integer.parseInt(args[0]);
	int value2 = Integer.parseInt(args[1]);
	int value3 = Integer.parseInt(args[2]);
	int tempvalue;

	tempvalue = (value1 < value2) ? value2 : value1;
	tempvalue = (value3 < tempvalue) ? tempvalue : value3;
	System.out.println("The greatest integer is: " + tempvalue);
	

    }
}
