package emre.shapes3d;

import emre.shapes.*;;

public class Cyclinder extends Circle {

	protected int height;
	
	public Cyclinder(int h, int r) {
		super(r);
		height = h; 
	}
	
	public double area() {
		return super.area() * 4;
	}
	
	public double volume() {
		return super.area() * height;
	}
	
	public String toString() {
		return "Height of Cyclinder is: " + height + 
				"\nRadius of Cyclinder is: " + super.radius;
	}
}
