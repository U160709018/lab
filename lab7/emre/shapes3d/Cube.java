package emre.shapes3d;

public class Cube {
	
	protected int side;
	
	public Cube(int s) {
		side = s;
	}
	
	public int area() {
		return 6 * side * side;
	}
	
	public int volume() {
		return side * side * side;
	}
	
	public String toString() {
		return "Side of cube is: " + side;
	}

}
